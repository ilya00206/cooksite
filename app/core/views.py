from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import redirect
from django.views.generic import FormView, ListView, CreateView, DetailView, UpdateView, DeleteView
from django.contrib.auth.models import User
from django.urls import reverse_lazy
from .forms import RegisterForm, CreateRecipeForm
from .mixins import AdminStaffRequiredMixin
from .models import Recipe


class IndexView(ListView):
    """ Index view """
    model = Recipe
    template_name = "core/index.html"

    def get_queryset(self):
        return Recipe.objects.all()


index_view = IndexView.as_view()


class RecipeApproveView(AdminStaffRequiredMixin, ListView):
    """ Approve recipe view """
    model = Recipe
    template_name = "core/recipe_approve.html"

    def get_queryset(self):
        return Recipe.objects.all()


recipe_approve_view = RecipeApproveView.as_view()


class RegisterView(FormView):
    """ Register form """
    template_name = 'registration/register.html'
    model = User
    form_class = RegisterForm
    success_url = reverse_lazy('core:index')

    def post(self, request, *args, **kwargs):
        response = super(RegisterView, self).post(request, *args, **kwargs)
        if response:
            form = self.get_form()
            form.save()
        return response


register_view = RegisterView.as_view()


class LoginView(FormView):
    """ Login form """
    template_name = 'registration/login.html'
    model = User
    form_class = RegisterForm
    success_url = reverse_lazy('core:index')

    def post(self, request, *args, **kwargs):
        response = super(LoginView, self).post(request, *args, **kwargs)
        if response:
            form = self.get_form()
            form.save()
        return response


register_view = RegisterView.as_view()


class RecipesView(ListView):
    """ Recipe list view """
    model = Recipe


recipes_view = RecipesView.as_view()


class RecipeDetailsView(DetailView):
    """ Recipe details view """
    template_name = 'core/recipe_details.html'
    model = Recipe


recipes_details = RecipeDetailsView.as_view()


class CreateRecipeView(LoginRequiredMixin, CreateView):
    """ Create author view """
    model = Recipe
    success_url = reverse_lazy('core:index')
    form_class = CreateRecipeForm
    login_url = reverse_lazy('login')


create_recipe_view = CreateRecipeView.as_view()


class DetailsRecipeApproveView(UpdateView):
    """Update recipe view """
    model = Recipe
    fields = ['name', 'time', 'image', 'ingredients', 'directions', 'is_approved']

    success_url = reverse_lazy('core:approve_recipe')


recipe_details_approve_view = DetailsRecipeApproveView.as_view()


class RecipeDeleteView(DeleteView):
    """ Delete recipe view """
    model = Recipe
    success_url = reverse_lazy('core:approve_recipe')


recipe_delete_view = RecipeDeleteView.as_view()


class RecipeAddToFavourites(LoginRequiredMixin, UpdateView):
    """ Add to favourites view """
    http_method_names = ['post', ]
    model = Recipe
    login_url = reverse_lazy('login')
    success_url = reverse_lazy('core:index')

    def post(self, *args, **kwargs):
        self.object = self.get_object()
        if self.object.favourite.filter(id=self.request.user.id).exists():
            self.object.favourite.remove(self.request.user)
        else:
            self.object.favourite.add(self.request.user)

        return redirect('core:recipe_details', pk=self.object.pk)


recipe_add_to_favourites_view = RecipeAddToFavourites.as_view()


class FavouriteRecipesView(LoginRequiredMixin, ListView):
    """Favourite list view"""
    model = Recipe
    template_name = "core/recipe_favourites.html"
    login_url = reverse_lazy('login')

    def get_queryset(self):
        return Recipe.objects.filter(favourite=self.request.user.id)


favourite_recipes_view = FavouriteRecipesView.as_view()
