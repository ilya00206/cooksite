from django.urls import path
from . import views

app_name = 'core'
urlpatterns = [
    path('', views.index_view, name='index'),
    path('register', views.register_view, name='register'),
    path('recipes', views.recipes_view, name='recipes'),
    path('recipe/approve', views.recipe_approve_view, name='approve_recipe'),
    path('recipe/create', views.create_recipe_view, name='create_recipe'),
    path('recipe_details/<int:pk>', views.recipes_details, name='recipe_details'),
    path('recipe/approve/<int:pk>', views.recipe_details_approve_view, name='recipe_details_approve'),
    path('recipe/delete/<int:pk>', views.recipe_delete_view, name='recipe_confirm_delete'),
    path('recipe/favourite/<int:pk>', views.recipe_add_to_favourites_view, name='favorite_recipe'),
    path('recipe/favourites', views.favourite_recipes_view, name='favourites'),
]
