from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.forms import ModelForm
from .models import Recipe


class RegisterForm(UserCreationForm):
    first_name = forms.CharField(max_length=30, required=False, help_text="Optional.", label="First name")
    last_name = forms.CharField(max_length=30, required=False, help_text="Optional.", label="Last name")
    email = forms.EmailField(max_length=64)

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2')


class CreateRecipeForm(ModelForm):

    class Meta:
        model = Recipe
        fields = "__all__"
        exclude = ['is_approved', 'favourite']

