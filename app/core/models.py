from django.contrib.auth import get_user_model
from django.db import models
from django.core.validators import MinLengthValidator, MaxValueValidator, MinValueValidator
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver


class Recipe(models.Model):
    name = models.CharField("Name", max_length=128, validators=[MinLengthValidator(2)])
    time = models.PositiveSmallIntegerField("Time", validators=[MaxValueValidator(2000),
                                                                MinValueValidator(1)])
    image = models.ImageField(null=True, blank=True)
    ingredients = models.TextField()
    directions = models.TextField()
    is_approved = models.BooleanField("Is approved", default=False)
    favourite = models.ManyToManyField(get_user_model(), related_name='favorite', blank=True)

    def __str__(self):
        return self.name


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()
