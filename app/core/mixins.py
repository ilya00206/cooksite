from django.contrib.auth.models import User
from faker import Faker
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin

class TestMixin:
    """ Test mixin """
    faker = Faker()

    def get_or_create_user(self):
        """ Get or create some user """
        name = self.faker.name()
        user, _ = User.objects.get_or_create(username=name, password=name)
        return user


class AdminStaffRequiredMixin(LoginRequiredMixin, UserPassesTestMixin):
    """ Check if user is admin of staff """
    def test_func(self):
        return self.request.user.is_superuser or self.request.user.is_staff
